import React from "react";
import Navbar from "../Navbar";
import Footer from "../Footer";
import Carousel from "../Carousel";

const Layout = ({ data, children }) => {
  return (
    <div>
      <Navbar navdata={data} />
      {children}
      <Footer />
      {/* <Carousel /> */}
    </div>
  );
};

export default Layout;
