import React from "react";
import Link from "next/link";
import { useStyles } from "./styles.js";
import {
  Card as CardBase,
  CardMedia,
  CardContent,
  Typography,
  CardActions,
  Grid,
} from "@material-ui/core";
import clsx from "clsx";

const Card = ({
  date,
  image,
  imageLink,
  grow,
  title,
  titleLeft,
  titlehref,
  titleLink,
  titleCenter,
  titleLinkCenter,
  titleCenterColor,
  titleLinkLeft,
  subTitle,
  content,
  icon,
  textalignleft,
  dangerouslySetInnerHTML,
  textcenter,
  textBelowContent,
  link,
  linkhref,
  boldLink,
  textWithlink,
  blogMedia,
}) => {
  // console.log(`cardData`, content);
  const classes = useStyles();
  return (
    <CardBase className={classes.root}>
      {image && (
        <div className={classes.hidden}>
          {/* Header */}
          {date && imageLink && (
            <div>
              {date && <span className={classes.badge}>{date}</span>}
              <Link href={imageLink}>
                <a>
                  <CardMedia
                    className={clsx(
                      classes.cardmedia,
                      { [classes.grow]: grow },
                      { [classes.blogCard]: blogMedia }
                    )}
                    image={image}
                  />
                </a>
              </Link>
            </div>
          )}
          {date && !imageLink && (
            <div>
              {date && <span className={classes.badge}>{date}</span>}
              <CardMedia
                className={clsx(
                  classes.cardmedia,
                  { [classes.grow]: grow },
                  { [classes.blogCard]: blogMedia }
                )}
                image={image}
              />
            </div>
          )}
          {!date && imageLink && (
            <div>
              <Link href={imageLink}>
                <a>
                  <CardMedia
                    className={clsx(classes.cardmedia, {
                      [classes.grow]: grow,
                    })}
                    image={image}
                  />
                </a>
              </Link>
            </div>
          )}
          {!date && !imageLink && (
            <CardMedia
              className={clsx(classes.cardmedia, { [classes.grow]: grow })}
              image={image}
            />
          )}
        </div>
      )}
      {/* Content */}
      {content && (
        <CardContent className={classes.cardcontent}>
          <div
            className={clsx(
              classes.title,
              { [classes.titleLeft]: titleLeft },
              { [classes.titleLink]: titleLink },
              { [classes.titleCenter]: titleCenter },
              { [classes.titleCenterColor]: titleCenterColor },
              { [classes.titleLinkCenter]: titleLinkCenter },
              { [classes.titleLinkLeft]: titleLinkLeft }
            )}
          >
            {titleLeft && <Typography variant="h5">{title}</Typography>}
            {titleCenterColor && <Typography variant="h5">{title}</Typography>}
            {titleCenter && <Typography variant="h5">{title}</Typography>}
            {titleLink && (
              <Link href={titlehref}>
                <a>{title}</a>
              </Link>
            )}
            {titleLinkCenter && (
              <Link href={titlehref}>
                <a>{title}</a>
              </Link>
            )}
            {titleLinkLeft && (
              <Link href={titlehref}>
                <a>{title}</a>
              </Link>
            )}
          </div>
          {subTitle && (
            <Typography className={classes.subtitle} variant="h5">
              {subTitle}
            </Typography>
          )}
          {content && !icon && typeof content === "object" && (
            <ul className={classes.textLeft}>
              {content.map((x, index) => (
                <li key={index}>{x}</li>
              ))}
            </ul>
          )}
          {content && textalignleft && (
            <Typography className={classes.textLeft}>{content}</Typography>
          )}
          {content && textcenter && (
            <Typography className={classes.textcenter}>{content}</Typography>
          )}
          {content && dangerouslySetInnerHTML && (
            <div
              dangerouslySetInnerHTML={{ __html: content }}
              className={clsx(classes.textLeft, {
                [classes.textcenter]: textcenter,
              })}
            />
          )}
          {content &&
            icon &&
            content.map((x, index) => (
              <Grid container className={classes.iconandcontent} key={index}>
                <div className={classes.icon}>
                  <Grid item lg={2}>
                    {icon[index]}
                  </Grid>
                </div>

                <Grid item lg={10} className={classes.gridtext}>
                  {content[index]}
                </Grid>
              </Grid>
            ))}

          {textBelowContent && (
            <Typography className={classes.textBelowContent} variant="h5">
              {textBelowContent}
            </Typography>
          )}
        </CardContent>
      )}

      {/* Footer */}
      {(link || boldLink || textWithlink) && (
        <CardActions
          className={clsx(
            classes.grayishpink,
            { [classes.boldLink]: boldLink },
            { [classes.link]: link || textWithlink }
          )}
        >
          {link && (
            <Link href={linkhref}>
              <a>
                <p className={classes.fontclass}>{link}</p>
              </a>
            </Link>
          )}
          {boldLink && (
            <Link href={linkhref}>
              <a>
                <Typography variant="h5">{boldLink}</Typography>
              </a>
            </Link>
          )}
          {textWithlink && (
            <>
              <p>Call us on :</p>
              <Link href={linkhref}>
                <a>
                  <p>{textWithlink}</p>
                </a>
              </Link>
            </>
          )}
        </CardActions>
      )}
    </CardBase>
  );
};

export default Card;
