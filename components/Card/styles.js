import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    height: "fit-content",
    backgroundColor: "#311D40",
    position: "relative",
  },
  cardmedia: {
    paddingTop: "56.25%",
    // backgroundSize: "contain"
    // backgroundColor: "#311D40",
  },
  // cardcontent: {
  //   height: "230px",
  // },
  hidden: {
    // border: "1px solid yellow",
    overflow: "hidden",
  },
  grow: {
    transition: "all .4s linear",
    "&:hover": {
      transform: "scale(1.2)",
    },
  },
  badge: {
    color: "white",
    backgroundColor: "red",
    top: 0,
    display: "block",
    fontWeight: 700,
    position: "absolute",
    textAlign: "center",
    padding: "10px 3px",
    zIndex: 1,
  },
  title: {
    textTransform: "uppercase",
    marginTop: 5,
    marginBottom: "1rem",
  },
  titleLeft: {
    textAlign: "left",
    color: "#ee018c",
  },
  titleCenter: {
    textAlign: "center",
    color: "white",
  },
  titleLinkCenter: {
    textAlign: "center",
    fontSize: "20px",
    color: "#ee018c",
    "&:hover": {
      color: "#eaad4e",
    },
  },
  titleLinkLeft: {
    textAlign: "left",
    fontSize: "20px",
    color: "#ee018c",
    "&:hover": {
      color: "#eaad4e",
    },
  },
  titleLink: {
    color: "#EE0567",
    fontSize: "120%",
    "&:hover": {
      textDecoration: "underline",
    },
  },
  titleCenterColor: {
    color: "#EE0567",
    textAlign: "center",
    textTransform: "uppercase",
    fontSize: "120%",
  },
  textcenter: {
    color: "#eaad4e",
    textAlign: "center",
    fontSize: "18px",
  },
  subtitle: {
    color: "#BD8D43",
    textAlign: "left",
    // fontSize: "25px",
  },
  textLeft: {
    color: "#eaad4e",
    textAlign: "left",
    fontSize: "19px",
    lineHeight: "25px",
  },
  textBelowContent: {
    color: "#EE0567",
    textAlign: "center",
    marginTop: "20px",
    marginBottom: 0,
  },
  icon: {
    marginRight: "5px",
    padding: "10px",
    borderRadius: "50%",
    border: "1px solid #eaad4e",
    display: "flex",
    alignItems: "center",
  },
  gridtext: {
    display: "flex",
    alignItems: "center",
  },
  iconandcontent: {
    marginBottom: "15px",
    color: "#eaad4e",
    // border: "1px solid red",
  },
  link: {
    color: "#EAAD4D",
    textTransform: "capitalize",
    display: "flex",
    justifyContent: "center",
    marginTop: 0,
    width: "100%",
    fontSize: "18px",
    "&:hover": {
      // textDecoration: "underline",
      color: "#EE0567",
    },
  },
  fontclass: {
    fontSize: "18px",
    margin: "5px 0px",
  },
  boldLink: {
    color: "#bd8d43",
    textTransform: "capitalize",
    display: "flex",
    justifyContent: "center",
  },
  grayishpink: {
    backgroundColor: "#562858",
  },
  carddiv: {
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap",
  },
  blogCard: {
    width: "100%",
    boder: "5px solid pink",
  },
});

export { useStyles };
