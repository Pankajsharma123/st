import { Snackbar as SnackbarBase } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";

export default function Snackbar({
  open,
  duration,
  onClose,
  elevation,
  variant,
  severity,
  children,
}) {
  return (
    <SnackbarBase open={open} autoHideDuration={duration} onClose={onClose}>
      <MuiAlert
        elevation={elevation || 6}
        variant={variant || "filled"}
        severity={severity}
        children={children}
      />
    </SnackbarBase>
  );
}
