import Image from "next/image";

function Image({ src, alt, ...props }) {
  if (!src || !alt) {
    return null;
  }
  return <Image src={src} alt={alt} {...props} />;
}

export default Image;
