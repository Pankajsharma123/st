import React from "react";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Card from "../Card";
import { LocationOn, Call } from "@material-ui/icons";

const useStyles = makeStyles({
  root: {
    // border: "1px solid yellow",
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "flex-start",
  },
});
const ThreeCard = ({
  data,
  therapies,
  location,
  deepInLocation,
  offers,
  celebTalk,
}) => {
  // console.log("+-+-+-+--+-+--+-+-+-+++");
  // console.log(typeof data);
  // console.log(data);
  const classes = useStyles();
  return (
    <Grid container spacing={3} className={classes.root}>
      {data.map((curr, index) => (
        <Grid item xs={12} sm={6} md={4} key={index}>
          {therapies && (
            <Card
              image={curr.image}
              grow={true}
              title={curr.name}
              titlehref={`/therapies/${curr.name.replace(/ /g, "-")}`}
              titleLinkCenter={true}
              content={curr.title}
              textcenter={true}
              textBelowContent={`Therapy starts at Rs.${curr.therapiestPrice} for ${curr.duration}`}
              link="Read More"
              linkhref={`/therapies/${curr.name.replace(/ /g, "-")}`}
            />
          )}
          {location && (
            <Card
              image={curr.image}
              grow={true}
              date={curr.ST_BRN}
              title={curr.ST_LOCN}
              titleCenterColor={true}
              subTitle="Amenities:"
              content={curr.amenities}
              link="Read More"
              linkhref={`/location/${curr.F_CITY}/${curr.ST_BRN}`}
            />
          )}

          {deepInLocation && (
            <Card
              image={curr.attractionImage}
              grow={true}
              title={curr.attractionName}
              titleLeft={true}
              content={curr.attractionDescription}
              textalignleft={true}
            />
          )}
          {offers && (
            <Card
              image={curr.branchImage}
              grow={true}
              date={curr.branchCode}
              title={curr.branchName}
              titleLinkLeft={true}
              titlehref={`/location/${curr.branchCity.toUpperCase()}/${
                curr.branchCode
              }`}
              icon={[<LocationOn />, <Call />]}
              content={[curr.branchAddress, curr.branchContactNumber]}
            />
          )}
          {celebTalk && (
            <Card
              image={curr.selebImage}
              title={curr.CelebName}
              titleCenterColor={true}
              content={curr.CelebComment}
              dangerouslySetInnerHTML={true}
            />
          )}
        </Grid>
      ))}
    </Grid>
  );
};

export default ThreeCard;
