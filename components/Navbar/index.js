import React from "react";
import Image from "next/image";
import Link from "next/link";
import { Container, AppBar, Toolbar, IconButton } from "@material-ui/core";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";
// import useMediaQuery from "@material-ui/core/useMediaQuery";

const useStyles = makeStyles((theme) => ({
  root: {
    // width: "100%",
    backgroundColor: "#311D40",
    position: "sticky",
    boxShadow: "1px 1px 15px #ee018c !important",
    flexGrow: 1,
    // top: 0,
    // border: "1px solid yellow",
  },

  navigationContainer: {
    display: "none",

    [theme.breakpoints.up("md")]: {
      display: "flex",
      alignItems: "center",
    },
    // border: "1px solid green",
  },
  navigationItemsContainer: {
    listStyleType: "none",
    textDecoration: "none",
    textTransform: "uppercase",
    fontSize: "0.8rem",
    // border: "1px solid blue",
  },
  navigationItems: {
    float: "left",
  },
  navigationItem: {
    display: "block",
    // padding: "4px",
    fontSize: "13px",
    fontWeight: 500,
    color: "#eaad4e",
    // border: "1px solid yellow",
    padding: "1rem 0.7rem",
    "&:hover": {
      color: "#ED158C",
    },
  },
  purpleOnText: {
    color: "#ED158C",
  },
  lastList: {
    backgroundColor: "#ED158C",
    color: "white",
    transition: "all .1s linear",
    "&:hover": {
      backgroundColor: "#311D40",
      color: "white",
    },
  },
  mobileview: {
    display: "flex",
    border: "1px solid red",
    // width: "100%",
    // float: "right",
    [theme.breakpoints.up("md")]: {
      display: "none",
      flexGrow: 1,
      border: "1px solid red",
    },
  },
  menuicon: {
    marginLeft: "auto",
    fontSize: "40px",
    color: "#ee018c",
  },
  b: {
    border: "1px solid red",
  },
  logo: {
    border: "1px solid red",
    [theme.breakpoints.down("md")]: {
      flexGrow: 1,
    },
  },
}));
const Navbar = ({ navdata }) => {
  // console.log("+++++++++++");
  // console.log(navdata);
  // const theme = useTheme();
  // const matches = useMediaQuery(theme.breakpoints.up("lg")); //theme is declared on its above line

  const classes = useStyles();

  const data = [
    ["home", "/home"],
    ["therapies", "/therapies"],
    ["about us", "/about-us"],
    ["location", "/location"],
    ["offers", "/offers"],
    ["corporate tie-up", "/corporate-tie-up"],
    ["card", "/cards"],
    ["priority cards", "/priority-cards"],
    ["buy gift cards", "/buy-gift-cards"],
    ["blogs", "/blogs"],
    ["contact", "/contact"],
    ["celeb talk", "/celeb-talk"],
    ["appointment", "/appointment"],
  ];

  const handleClick = () => {};
  return (
    <AppBar className={classes.root}>
      <Container maxWidth="lg">
        <Toolbar>
          <div className={classes.logo}>
            <Link href={"/home"}>
              <a>
                <Image
                  src="/images/sukhothai-logo.png"
                  width={151}
                  height={52}
                />
              </a>
            </Link>
          </div>

          <div className={classes.navigationContainer}>
            <ul className={classes.navigationItemsContainer}>
              {data.map(([name, url], index) => {
                if (name == "home") {
                  return (
                    <li className={classes.navigationItems} key={index}>
                      <Link href={url}>
                        <a
                          className={clsx(classes.navigationItem, {
                            [classes.purpleOnText]: true,
                          })}
                        >
                          {name}
                        </a>
                      </Link>
                    </li>
                  );
                } else if (name === "appointment") {
                  return (
                    <li className={classes.navigationItems} key={index}>
                      <Link href={url}>
                        <a
                          className={clsx(classes.navigationItem, {
                            [classes.lastList]: true,
                          })}
                        >
                          {name}
                        </a>
                      </Link>
                    </li>
                  );
                } else {
                  return (
                    <li className={classes.navigationItems} key={index}>
                      <Link href={url}>
                        <a className={classes.navigationItem}>{name}</a>
                      </Link>
                    </li>
                  );
                }
              })}
            </ul>
          </div>
          <div className={classes.mobileview}>
            <IconButton onClick={handleClick}>
              <MenuIcon className={classes.menuicon} />
            </IconButton>
          </div>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default Navbar;
