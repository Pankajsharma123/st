import React from "react";
import Card from "../Card";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  b: {
    // border: "1px solid yellow",
  },
}));
export default function index({ data, blogs }) {
  // console.log(`data from blogs`, data);
  const classes = useStyles();
  return (
    <div>
      <Grid container spacing={2}>
        {data &&
          data.map((curr, index) => {
            let date = new Date(curr.date);
            date = `${date.toLocaleString("default", { month: "short" })} ${(
              "0" + date.getDate()
            ).slice(-2)}`; //if we are getting 1 digit then that creates problem so we r adding "0" at left and do slice so it take's last two digit's
            return (
              <Grid item lg={6} key={index}>
                {blogs && (
                  <Card
                    image={curr.image}
                    imageLink={`/blogs/${curr.title.replace(/ /g, "-")}`}
                    grow={true}
                    date={date}
                    content={
                      curr.excerpt.length > 250
                        ? `${curr.excerpt.slice(0, 250)}....`
                        : curr.excerpt
                    }
                    titleLinkLeft={true}
                    titlehref={`/blogs/${curr.title.replace(/ /g, "-")}`}
                    title={curr.title}
                    textalignleft={true}
                    link={`Read More`}
                    linkhref={`/blogs/${curr.title.replace(/ /g, "-")}`}
                  />
                )}
              </Grid>
            );
          })}
      </Grid>
    </div>
  );
}
