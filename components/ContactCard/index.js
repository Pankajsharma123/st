import Link from "next/link";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { LocationOn, Mail, Call } from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {
    //     border: "1px solid red",
    backgroundColor: "#311D40",
    display: "flex",
    justifyContent: "center",
  },
  b: {
    border: "1px solid red",
  },
  circle: {
    borderRadius: "100%",
    border: "2px solid #eaad4e",
    display: "flex",
    justifyContent: "center",
    width: "80px",
    height: "80px",
    padding: "15px",
    margin: "0 auto",
  },
  icon: {
    color: "#EAAD4E",
    height: "50px",
    width: "50px",
  },
  address: {
    width: "100%",
    color: "#eaad4e",
    textAlign: "center",
    marginTop: "5px",
    fontSize: "1.2rem",
  },
  contactNumber: {
    width: "100%",
    color: "#eaad4e",
    textAlign: "center",
    marginTop: "10px",
    fontSize: "1.2rem",
    transition: "0.5s linear",
    "&:hover": {
      color: "#ee018c",
      textDecorationLine: "underline",
    },
  },
}));
const ContactCard = ({
  locationicon,
  mailicon,
  callicon,
  address,
  gmail,
  contact,
}) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div>
        <div className={classes.circle}>
          {locationicon && <LocationOn className={classes.icon} />}
          {mailicon && <Mail className={classes.icon} />}
          {callicon && <Call className={classes.icon} />}
        </div>

        {address && (
          <Typography className={classes.address}>{address}</Typography>
        )}

        {gmail && <Typography className={classes.address}>{gmail}</Typography>}

        {contact && (
          <div className={classes.address}>
            <span>{`Please call us: `}</span>
            <span className={classes.contactNumber}>
              <Link href={`tel: ${contact}`}>
                <a>{contact}</a>
              </Link>
            </span>
          </div>
        )}
      </div>
    </div>
  );
};

export default ContactCard;
