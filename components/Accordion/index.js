import React, { useState } from "react";
import {
  Accordion as AccordionBase,
  AccordionSummary,
  AccordionDetails,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ThreeCard from "../ThreeCard";

const useStyles = makeStyles({
  root: {
    backgroundColor: "#1E1228",
    color: "#EAAD4E",
    // border: "1px solid yellow",
  },
  iconColor: {
    color: "#EAAD4E",
  },
  accordionsummary: {
    borderBottom: "1px solid #EAAD4E",
    height: "55px",
  },
});
const Accordion = ({ data, expandall, location }) => {
  // console.log("/////////////////////");
  // console.log(data);
  const [expand, setExpand] = useState(false);
  const handleChange = (ind) => (event, isExpanded) => {
    return setExpand(isExpanded ? ind : false);
  };
  const classes = useStyles();
  return (
    <div>
      {data.map((x, index) => (
        // Head
        <AccordionBase
          className={classes.root}
          expanded={expand === index || expandall}
          onChange={handleChange(index)}
          key={index}
        >
          <AccordionSummary
            className={clsx({ [classes.accordionsummary]: location })}
            expandIcon={<ExpandMoreIcon className={classes.iconColor} />}
          >
            {location && x._id.city.toUpperCase()}
          </AccordionSummary>
          <AccordionDetails>
            {location && <ThreeCard data={x.DATA} location={true} />}
          </AccordionDetails>
        </AccordionBase>
      ))}
    </div>
  );
};

export default Accordion;
