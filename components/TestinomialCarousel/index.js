import React, { useState } from "react";
import Rating from "@material-ui/lab/Rating";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import {
  Card,
  CardActionArea,
  CardContent,
  Typography,
  Box,
  CardActions,
  CardMedia,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    width: 345,
    border: "2px solid #1F1229",
    backgroundColor: "#311D40",
    color: "#BD8D43",
    textAlign: "center",
    marginTop: "50px",
    position: "relative",
    overflow: "visible",
  },
  cardMedia: {
    position: "absolute",
    border: "1px solid #BD8D43",
    borderRadius: "50%",
    top: "-45px",
    width: "88px",
    height: "88px",
    left: "120px",
    backgroundColor: "white",
    padding: "5px",
  },
  text: {
    marginTop: "33px",
  },
  rating: {
    color: "#BD8D43",
    //     border: "1px solid green",
    marginTop: "15px",
    marginBottom: "20px",
  },
  cardbottom: {
    backgroundColor: "#1F1229",
  },
  bottomText: {
    width: "100%",
    textAlign: "right",
    textTransform: "capitalize",
  },
});
const TestinomialCarousel = () => {
  //   const [rating, setRating] = useState(2);
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.cardMedia}
          component="img"
          alt="image"
          image="https://www.sukhothai.in/wp-content/themes/sukhothai-live/images/sukhothai-flower.png"
        />
        <CardContent>
          <Typography className={classes.text}>
            Impressed a lot, will Recommend to others for sure
          </Typography>
          <Box>
            <Rating
              className={classes.rating}
              readOnly
              precision={0.5}
              value={3.5}
              emptyIcon={<StarBorderIcon fontSize="inherit" />}
            />
          </Box>
        </CardContent>
      </CardActionArea>
      <CardActions className={classes.cardbottom}>
        <Typography className={classes.bottomText}>ryan</Typography>
      </CardActions>
    </Card>
  );
};

export default TestinomialCarousel;
