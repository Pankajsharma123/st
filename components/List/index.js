import Link from "next/link";
import Image from "next/image";
import {
  List as ListBase,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  // root: {
  //   backgroundColor: "#1F1229",
  // },
  image: {
    // border: "1px solid red",
    marginRight: "8px",
  },
  listitem: {
    // border: "1px solid blue",
    display: "flex",
    justifyContent: "space-between",
    padding: 0,
  },
  listitemtext: {
    width: "100%",
    height: "100%",
    // border: "1px solid red",
    marginTop: "0px",
    marginBottom: "0px",
    padding: 0,
  },
  listitemtextprimary: {
    // border: "1px solid pink",
    marginBottom: "15px",
    fontSize: "20px",
    color: "#98789a",
    "&:hover": {
      color: "#eaad4e",
    },
  },
  listitemtextsecondary: {
    color: "#98789a",
  },
}));
const List = ({ currData, index, date }) => {
  const classes = useStyles();
  return (
    <ListBase key={index} className={classes.root}>
      <ListItem className={classes.listitem}>
        <ListItemAvatar className={classes.image}>
          <Link
            href={`/blogs/${decodeURIComponent(currData.title).replace(
              / /g,
              "-"
            )}`}
          >
            <a>
              <Image src={currData.image} width={150} height={100} />
            </a>
          </Link>
        </ListItemAvatar>
        <ListItemText
          primary={
            <div className={classes.listitemtextprimary}>
              <Link
                href={`/blogs/${decodeURIComponent(currData.title).replace(
                  / /g,
                  "-"
                )}`}
              >
                <a>{currData.title}</a>
              </Link>
            </div>
          }
          secondary={
            <Typography className={classes.listitemtextsecondary}>
              {date}
            </Typography>
          }
          className={classes.listitemtext}
        />
      </ListItem>
    </ListBase>
  );
};

export default List;
