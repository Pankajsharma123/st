import React from "react";
import { Grid, Typography, Divider } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    // border: "1px solid red",
    paddingTop: "15px",
    paddingBottom: "25px",
    marginBottom: "20px",
    // backgroundColor: "#311D40"
  },
  textCenter: {
    // border: "1px solid yellow",
    textAlign: "center",
    textTransform: "capitalize",
    color: "#ee018c",
    fontSize: "2.3rem",
    [theme.breakpoints.down("md")]: {
      fontSize: "1.5rem",
    },
  },
  horizontalLine: {
    backgroundColor: "#ee018c",
    width: "7%",
    marginTop: "6px",
    marginBottom: "8px",
    height: "1px",
    border: "none",
  },
  info: {
    // border: "1px solid red",
    textAlign: "center",
    color: "#eaad4e",
  },
}));
const Titleblock = ({ title, baseTitle }) => {
  const classes = useStyles();
  return (
    <Grid container className={classes.root}>
      <Grid item xs={12}>
        {title && (
          <Typography variant="h3" className={classes.textCenter}>
            {title}
          </Typography>
        )}
        <hr className={classes.horizontalLine} />
        {baseTitle && (
          <Typography variant="h6" className={classes.info}>
            {baseTitle}
          </Typography>
        )}
      </Grid>
    </Grid>
  );
};

export default Titleblock;
