import React from "react";
import Link from "next/link";
import DoneIcon from "@material-ui/icons/Done";
import FacebookIcon from "@material-ui/icons/Facebook";
import YouTubeIcon from "@material-ui/icons/YouTube";
import InstagramIcon from "@material-ui/icons/Instagram";
import TwitterIcon from "@material-ui/icons/Twitter";

import { Grid, Paper, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#1F1229",
    // position: "absolute",
    bottom: 0,
    width: "100%",
    display: "flex",
    flexDirection: "column",
    // paddingLeft: 120,
    // paddingRight: 120,
    padding: "46px 0px 10px 44px",
  },
  contaniner: {
    display: "flex",
    flexDirection: "row",
    // marginBottom: "20px",
    // paddingBottom:"20px"
  },
  copyrightContainer: {
    // border: "1px solid yellow",
    backgroundColor: "#1F1229",
    marginTop: "20px",
    marginBottom: "20px",
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    // border: "1px solid red",
    // pading: "46px 0px 10px 44px",
    backgroundColor: "#1F1229",
  },
  title: {
    marginTop: 10,
    marginBottom: 10,
    textTransform: "uppercase",
    color: "#EE0567",
  },
  textcenter: {
    color: "#bd8d43",
    textAlign: "center",
  },
  textleft: {
    color: "#bd8d43",
    textAlign: "left",
  },
  list: {
    paddingLeft: 0,
    listStyle: "none",
  },
  hover: {
    display: "flex",
    marginBottom: "5px",
    color: "#bd8d43",
    "&:hover": {
      color: "#EE0567",
    },
  },
  icon: {
    width: "20px",
    height: "20px",
  },
  spaceBetween: {
    marginLeft: "5px",
  },
  row: {
    display: "flex",
    flexDirection: "row",
    marginTop: 0,
    paddingLeft: 0,
    listStyle: "none",
  },
  circle: {
    backgroundColor: "#311D40",
    borderRadius: "50%",
    marginRight: "5px",
    padding: "5px",
    border: "2px solid transparent",
    transition: "all .1s linear",
    "&:hover": {
      border: "2px solid #EE0567",
    },
  },
  vividPink: {
    color: "#EE0567",
  },
}));
const Footer = () => {
  const classes = useStyles();
  const usefulLinks = [
    ["Home", "/home/"],
    ["About Sukho Thai", "/about-us"],
    ["Blog", "/blogs/"],
    ["Location Maps", "/location/"],
    ["Benefits", "/benefits/"],
    ["Contact us", "/contact/"],
  ];

  const socialMedia = [
    [<FacebookIcon />, "https://www.facebook.com/MySukhoThai"],
    [
      <YouTubeIcon />,
      "https://www.youtube.com/channel/UCpPdd1kD69uIISwf1IkNqFQ",
    ],
    [<InstagramIcon />, "https://instagram.com/mysukhothai/"],
    [<TwitterIcon />, "https://twitter.com/mysukhothai"],
  ];
  return (
    <div className={classes.root}>
      <Grid container className={classes.contaniner}>
        <Grid item xs>
          <Paper className={classes.paper}>
            <Typography className={classes.title} variant="h5">
              about us
            </Typography>
            <Typography className={classes.textleft} variant="body1">
              Established in 2010, Sukho Thai is India's premier Foot Therapy
              brand inspired from the deeply rooted cultures of Thailand. The
              seemingly small nation has a great culture and is home to exotic
              fruits and flowers. The word ‘Sukho’ is derived from the Sanskrit
              word ‘Sukh’ which connotes happiness and ‘Thai’ reflects its
              innate bond with Thailand. With over 21 SukhoThai Spanning across
              Mumbai ,Pune ,Goa ,Bengaluru and Kathmandu. Sukho Thai is the
              first international foot massages spa chain to open a series of
              outlets in India.
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs>
          <Paper className={classes.paper}>
            <Typography className={classes.title} variant="h5">
              userful link
            </Typography>

            <ul className={classes.list}>
              {usefulLinks.map(([name, link], index) => (
                <li className={classes.hover} key={index}>
                  <DoneIcon className={classes.icon} />
                  <Link href={link}>
                    <a className={classes.spaceBetween}>{name}</a>
                  </Link>
                </li>
              ))}
            </ul>
          </Paper>
        </Grid>
        <Grid item xs>
          <Paper className={classes.paper}>
            <Typography className={classes.title} variant="h5">
              social media
            </Typography>
            <ul className={classes.row}>
              {socialMedia.map(([mediaName, mediaLink], index) => (
                <li className={classes.circle} key={index}>
                  <Link href={mediaLink}>
                    <a className={classes.vividPink}>{mediaName}</a>
                  </Link>
                </li>
              ))}
            </ul>
          </Paper>
        </Grid>
      </Grid>
      <Grid container className={classes.copyrightContainer}>
        <Grid item xs>
          <Typography className={classes.textcenter}>
            Copyrights © 2018 - Sukho Thai India Pvt. Ltd. - All Rights Reserved
          </Typography>
        </Grid>
      </Grid>
    </div>
  );
};

export default Footer;
