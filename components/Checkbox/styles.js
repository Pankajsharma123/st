import { makeStyles } from "@material-ui/core";

const useStyle = makeStyles({
  container: {
    // border: "1px solid yellow",
    // padding: "0px",
  },
  root: {
    color: "#eaad4e",
  },

  label: {
    color: "#eaad4e",
    marginRight: "5px",
    // border: "1px solid yellow",
  },
});

export { useStyle };
