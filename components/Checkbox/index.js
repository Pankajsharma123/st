import {
  FormControl,
  FormControlLabel,
  Checkbox as CheckboxBase,
} from "@material-ui/core";
import { useStyle } from "./styles";

export default function Checkbox({ checked, name, label, onChange }) {
  console.log(`checked`, checked, name, label);

  const convertToDefaultEventParameter = (name, value) => ({
    //here we r override property inside of an object i.e (e)
    target: { name: name, value: value },
  });

  //==========or==========
  // const convertToDefEventPara = (name, value) => ({
  //   target: {
  //     name,
  //     value,
  //   },
  // });

  const classes = useStyle();
  return (
    <FormControl className={classes.container}>
      <FormControlLabel
        control={
          <CheckboxBase
            checked={checked || false}
            name={name}
            onChange={(e) =>
              onChange(convertToDefaultEventParameter(name, e.target.checked))
            }
            className={classes.root}
          />
        }
        label={label}
        className={classes.label}
      />
    </FormControl>
  );
}
