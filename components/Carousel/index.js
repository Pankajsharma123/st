import React from "react";
import CarouselBase from "react-material-ui-carousel";
import { Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    width: "100%",
    backgroundColor: "green"
  },
  media: {
    width: "100%"
  }
});
function Item({ item }) {
  const classes = useStyles();
  return <img src={item} alt="Sukhothai" className={classes.media} />;
}

const Carousel = () => {
  var items = [
    "https://www.sukhothai.in/wp-content/uploads/2018/09/Sukhothai-spa-bengaluru-des.jpg",
    "https://www.sukhothai.in/wp-content/uploads/2020/12/10-years.jpg",
    "https://www.sukhothai.in/wp-content/uploads/2021/01/One-hour-vacation.jpg"
  ];

  return (
    <CarouselBase>
      {items.map((item, i) => (
        <Item key={i} item={item} />
      ))}
    </CarouselBase>
  );
};

export default Carousel;
