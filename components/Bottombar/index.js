import React, { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { BottomNavigation, BottomNavigationAction } from "@material-ui/core";
import HomeIcon from "@material-ui/icons/Home";
import CardGiftcardIcon from "@material-ui/icons/CardGiftcard";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import ScheduleIcon from "@material-ui/icons/Schedule";
import LocalOfferIcon from "@material-ui/icons/LocalOffer";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    position: "fixed",
    width: "100%",
    bottom: 0,
    // border: "1px solid red",
    display: "flex",
    justifyContent: "space-between",
    overflow: "hidden",
  },
  innerContainer: {
    // border: "1px solid blue"
  },
});
const Bottombar = () => {
  const router = useRouter();
  const [value, setValue] = useState(0);
  const classes = useStyles();

  const data = [
    ["Home", <HomeIcon />, "/home"],
    ["Cards", <CardGiftcardIcon />, "/cards"],
    ["Appointment", <ScheduleIcon />, "/appointment"],
    ["Location", <LocationOnIcon />, "/location"],
    ["Offers", <LocalOfferIcon />, "/offers"],
  ];

  const handleChange = (e, newValue) => {
    //(newValue) is comming from BottomNavigationAction
    setValue(newValue);
  };
  return (
    <BottomNavigation
      className={classes.root}
      value={value}
      showLabels
      onChange={(e, newValue) => handleChange(e, newValue)}
    >
      {data &&
        data.map(([name, icon, link], index) => (
          <BottomNavigationAction
            className={classes.innerContainer}
            key={index}
            label={name}
            icon={icon}
            onClick={() => router.push(link)}
          />
        ))}
    </BottomNavigation>
  );
};

export default Bottombar;
