import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    width: "100%",
    marginBottom: "10px",
    overflow: "hidden",
  },
  tableHeadCell: {
    color: "#eaad4e",
    //     border: "1px solid red",
    textAlign: "center",
    backgroundColor: "#562858",
    borderBottom: "1px solid #562858",
  },
  tableBodyCell: {
    color: "#eaad4e",
    textAlign: "center",
    border: "1px solid #562858",
    backgroundColor: "#311D40",
  },
  changeCellColor: {
    color: "#DC1379",
  },
  radio: {
    color: "#eaad4e",
  },
});

export { useStyles };
