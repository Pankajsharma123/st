import {
  TableContainer,
  Table as TableBase,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper,
  Radio,
} from "@material-ui/core";
import { useStyles } from "./styles.js";
import clsx from "clsx";

const Table = ({ headData, cellData, handleSelect, selectedValue }) => {
  // console.log(`headData`, headData);
  console.log(`cellData Table`, typeof cellData);
  console.log(`cellData Table`, cellData);

  const classes = useStyles();
  return (
    <TableContainer className={classes.root}>
      <TableBase>
        <TableHead>
          <TableRow>
            {headData.map((head, index) => (
              <TableCell key={index} className={classes.tableHeadCell}>
                {head}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {cellData.length > 1 ? (
            cellData.map((x, index) => (
              <TableRow key={index}>
                <TableCell
                  className={clsx(classes.tableBodyCell, {
                    [classes.changeCellColor]:
                      selectedValue === index.toString(),
                  })}
                >
                  {index + 1}
                </TableCell>
                <TableCell
                  className={clsx(classes.tableBodyCell, {
                    [classes.changeCellColor]:
                      selectedValue === index.toString(),
                  })}
                >
                  {x.F_AMT}
                </TableCell>
                <TableCell
                  className={clsx(classes.tableBodyCell, {
                    [classes.changeCellColor]:
                      selectedValue === index.toString(),
                  })}
                >
                  {x.F_VAL}
                </TableCell>
                <TableCell
                  className={clsx(classes.tableBodyCell, {
                    [classes.changeCellColor]:
                      selectedValue === index.toString(),
                  })}
                >
                  {`${x.F_PER}%`}
                </TableCell>
                <TableCell
                  className={clsx(classes.tableBodyCell, {
                    [classes.changeCellColor]:
                      selectedValue === index.toString(),
                  })}
                >
                  <Radio
                    checked={selectedValue === index.toString()}
                    onChange={handleSelect}
                    value={index.toString()}
                    className={classes.radio}
                  />
                </TableCell>
              </TableRow>
            ))
          ) : (
            <TableRow>
              <TableCell className={classes.tableBodyCell}>
                {cellData[0].planIndex}
              </TableCell>
              <TableCell className={classes.tableBodyCell}>
                {cellData[0].F_AMT}
              </TableCell>
              <TableCell className={classes.tableBodyCell}>
                {cellData[0].F_VAL}
              </TableCell>
              <TableCell
                className={classes.tableBodyCell}
              >{`${cellData[0].F_PER}%`}</TableCell>
            </TableRow>
          )}
        </TableBody>
      </TableBase>
    </TableContainer>
  );
};

export default Table;
