import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    width: (props) => props.width,
    height: (props) => props.height,
    color: (props) => props.color,
    textTransform: (props) =>
      props.textTransform ? props.textTransform : "none",
    borderRadius: (props) => props.borderRadius,
    background: (props) => props.background,
    "&:hover": {
      background: (props) => props.hoverBackground,
    },
  },
}));

export default function ButtonComponent({
  variant,
  label,
  size,
  type,
  onClick,
  ...props
}) {
  const classes = useStyles(props);
  return (
    <Button
      variant={variant || ""}
      size={size || "large"}
      type={type}
      onClick={onClick}
      className={classes.root}
    >
      {label}
    </Button>
  );
}
