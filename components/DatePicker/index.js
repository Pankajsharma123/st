import DateFnsUtils from "@date-io/date-fns";
import { Grid } from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  subGridContainer: {
    display: "flex",
    flexDirection: "row",
    // border: "1px solid red",
  },
  datePicker: {
    "& .MuiFormHelperText-root": {
      color: "#F44336",
    },
  },
}));

export default function DatePicker({
  date,
  label,
  name,
  value,
  onChange,
  error,
}) {
  const convertToDefEventPara = (name, date) => ({
    //if u see here we are passing target property in object and it is overriding target property of handleChange mehtod
    //on handleChange we access it by e.target.name and e.target.value
    target: { name: name, value: date },
  });

  const classes = useStyles();

  return (
    <Grid item lg={6} className={classes.subGridContainer}>
      {date && date}
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
          variant="inline"
          inputVariant="outlined"
          format="dd/MM/yyyy"
          margin="dense"
          label={label}
          name={name}
          value={value}
          onChange={(date) => onChange(convertToDefEventPara(name, date))}
          autoOk={true}
          fullWidth
          autoComplete="off"
          helperText={undefined || error}
          className={classes.datePicker}
        />
      </MuiPickersUtilsProvider>
    </Grid>
  );
}
