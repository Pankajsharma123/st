import { Grid, TextField } from "@material-ui/core";
// import { Person, Call, LocationOn, Mail } from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  subcontainer: {
    display: "flex",
    flexDirection: "row",
    marginBottom: theme.spacing(1),
    // border: "1px solid red",
    // justifyContent: "center",
  },
  textfield: {
    color: "#eaad4e",
  },
}));
export default function TextFieldComponent({
  lg,
  md,
  sm,
  person,
  call,
  location,
  mail,
  message,
  label,
  name,
  value,
  onChange,
  placeholder,
  variant,
  size,
  type,
  multiline,
  isFullWidth,
  rows,
  error = null,
}) {
  // console.log(`error`, error);
  const classes = useStyles();
  return (
    <Grid
      item
      lg={lg ?? 12}
      md={md ?? 12}
      sm={sm ?? 12}
      className={classes.subcontainer}
    >
      {person && person}
      {call && call}
      {location && location}
      {mail && mail}
      {message && message}
      <TextField
        label={label}
        name={name}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        variant={variant}
        InputProps={{
          className: classes.textfield,
        }}
        size={size}
        type={type || "text"}
        fullWidth={isFullWidth}
        multiline={multiline || false}
        rows={rows || undefined}
        // error={false || Boolean(error)}
        // helperText={undefined || error}
        {...(error && { error: true, helperText: error })}
      />
    </Grid>
  );
}
