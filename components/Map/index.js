import React, { useState } from "react";
import Link from "next/link";
import {
  GoogleMap,
  withScriptjs,
  withGoogleMap,
  Marker,
  InfoWindow,
} from "react-google-maps";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  link: {
    color: "#ee018c",
    "&:hover": {
      color: "#eaad4e",
    },
  },
  fontsize: {
    fontSize: "16px",
  },
});
const Mymap = withScriptjs(
  //withGoogleMap accept callback function
  withGoogleMap(({ latitude, longitude, title, address, contact, url }) => {
    const classes = useStyles();

    const [click, setClick] = useState(false);
    const handleClick = () => {
      setClick(!click);
    };
    return (
      <GoogleMap
        defaultZoom={5}
        defaultCenter={{ lat: latitude, lng: longitude }}
      >
        <Marker
          position={{ lat: latitude, lng: longitude }}
          onClick={handleClick}
        >
          {click && (
            <InfoWindow onCloseClick={handleClick}>
              <div
                style={{
                  padding: `5px`,
                }}
              >
                <div className={classes.fontsize}>
                  <strong>{title}</strong>
                  <p>{address}</p>
                  <p>
                    <strong>Phone:- </strong>
                    {`+${contact}`}
                  </p>
                  <Link href={url}>
                    <a
                      className={classes.link}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Direction
                    </a>
                  </Link>
                </div>
              </div>
            </InfoWindow>
          )}
        </Marker>
      </GoogleMap>
    );
  })
);

export default function Map({
  latitude,
  longitude,
  title,
  address,
  contact,
  url,
}) {
  return (
    <Mymap
      latitude={latitude}
      longitude={longitude}
      title={title}
      address={address}
      contact={contact}
      url={url}
      googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEYS}`}
      loadingElement={<div style={{ height: `100%` }} />}
      containerElement={<div style={{ height: `100%` }} />}
      mapElement={<div style={{ height: `100%` }} />}
    />
  );
}
