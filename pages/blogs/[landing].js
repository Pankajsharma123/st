import React from "react";
import { Card, Titleblock, List } from "../../components";
import { Container, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  banner: {
    width: "100%",
  },
  // b: {
  //   border: "1px solid red",
  // },
  content: {
    color: "white",
    backgroundColor: "#1F1229",
  },
  scrollContainer: {
    backgroundColor: "#1F1229",
    height: "920px",
    // border: "1px solid red",
  },
  scroll: {
    // border: "1px solid red",
    height: "800px",
    overflow: "auto",
    marginTop: "20px",
  },
  tulipColorForText: {
    color: "#eaad4d",
  },
  tulipColorForLine: {
    backgroundColor: "#eaad4d",
    height: "1px",
    border: "none",
    // width: "7%",
  },
}));
const BlogsLandingPage = ({ bannerData, blogLandingData, blogList }) => {
  const classes = useStyles();
  // console.log(`blogLandingData`, blogLandingData);

  let date = new Date(blogLandingData.result.date);
  date = `${date.toLocaleString("default", { month: "short" })} ${(
    "0" + date.getDate()
  ).slice(-2)}`; //if we are getting 1 digit then that creates problem so we r adding "0" at left and do slice so it take's last two digit's
  // console.log(`date`, date);
  return (
    <div className={classes.root}>
      <img
        src="/images/page-banner/health-benefits.jpg"
        className={classes.banner}
      />
      <Container maxWidth="lg">
        <Titleblock title={bannerData.data[5].pageHeader} />
        <Grid container spacing={4}>
          <Grid item lg={8}>
            <Card
              image={blogLandingData.result.image}
              date={date}
              blogMedia={true}
              title={blogLandingData.result.title}
              titleLeft={true}
              content={true}
              content={blogLandingData?.result.blog[0]?.description}
              dangerouslySetInnerHTML={true}
            />
          </Grid>
          <Grid item lg={4} className={classes.scrollContainer}>
            <Typography variant="h4" className={classes.tulipColorForText}>
              Popular Post
            </Typography>
            <hr className={classes.tulipColorForLine} />
            <div className={classes.scroll}>
              {blogList.map((x, index) => {
                let date = new Date(x.date);
                date = `${date.toLocaleString("default", { month: "long" })} ${(
                  "0" + date.getDate()
                ).slice(-2)}, ${date.getFullYear()}`; //if we are getting 1 digit then that creates problem so we r adding "0" at left and do slice so it take's last two digit's
                return <List currData={x} index={index} date={date} />;
              })}
            </div>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export async function getStaticPaths() {
  const res = await fetch("http://login.sukhothai.in/route/blogST/blogList");
  const data = await res.json();

  const paths = data.map((x) => ({
    // see that map returns list of object therefor ({})
    params: { landing: decodeURIComponent(x?.title).replace(/ /g, "-") },
  }));

  return { paths, fallback: false };
}
export async function getStaticProps({ params }) {
  const res1 = await fetch(
    "http://login.sukhothai.in/route/webBannerST/getAllWebPagesBannerData"
  );

  const res2 = await fetch(
    `http://login.sukhothai.in/route/blogST/getBlogByName/${encodeURIComponent(
      params.landing.replace(/-/g, " ")
    )}`
  );

  const res3 = await fetch("http://login.sukhothai.in/route/blogST/blogList");

  const bannerData = await res1.json();
  const blogLandingData = await res2.json();
  const blogList = await res3.json();

  if (!blogLandingData && !bannerData && !blogList) {
    return { notFound: true };
  }

  return { props: { bannerData, blogLandingData, blogList } };
}
export default BlogsLandingPage;
