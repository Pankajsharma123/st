import React, { useEffect, useState } from "react";
import { Container, Grid, Typography, Divider } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import { List, Titleblock, TwoCard } from "../../components";
import { Pagination } from "@material-ui/lab";
import { yellow } from "@material-ui/core/colors";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  banner: {
    width: "100%",
  },
  b: {
    // border: "1px solid red",
  },

  pagination: {
    marginTop: "50px",
    marginBottom: "100px",
    // border: "1px solid blue",
    "& .MuiPaginationItem-rounded": {
      color: "#ED158C",
      "&:hover": {
        color: "white",
        backgroundColor: "#ED158C",
      },
    },
    "& .MuiPaginationItem-outlined": {
      // border: "1px solid #ED158C",
    },
  },
  scrollContainer: {
    backgroundColor: "#1F1229",
    height: "920px",
  },
  scroll: {
    // border: "1px solid red",
    height: "800px",
    overflow: "auto",
    marginTop: "20px",
  },
  tulipColorForText: {
    color: "#eaad4d",
  },
  tulipColorForLine: {
    backgroundColor: "#eaad4d",
    height: "1px",
    border: "none",
    // width: "7%",
  },
}));
export default function Blogs({ blogsData, bannerData }) {
  const [items, setItems] = useState(blogsData.slice(0, 6));

  const handleChange = (event, value) => {
    console.log("hi");
    window.scrollTo({ top: 0, behavior: "smooth" });
    const clickData = blogsData.slice(value * 6 - 6, value * 6);
    return setItems(clickData);
  };

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <img src={bannerData.data[5].bannerImage} className={classes.banner} />
      <Container maxWidth="lg" className={classes.b}>
        <Titleblock title={bannerData.data[5].pageHeader} />
        <Grid container className={classes.b} spacing={6}>
          <Grid item lg={8}>
            <TwoCard data={items} blogs={true} />
          </Grid>
          <Grid item lg={4} className={classes.scrollContainer}>
            <Typography variant="h4" className={classes.tulipColorForText}>
              Popular Post
            </Typography>
            <hr className={classes.tulipColorForLine} />
            <div className={classes.scroll}>
              {blogsData.map((x, index) => {
                let date = new Date(x.date);
                date = `${date.toLocaleString("default", { month: "long" })} ${(
                  "0" + date.getDate()
                ).slice(-2)}, ${date.getFullYear()}`; //if we are getting 1 digit then that creates problem so we r adding "0" at left and do slice so it take's last two digit's
                return <List currData={x} index={index} date={date} />;
              })}
            </div>
          </Grid>
          <Grid item lg={12}>
            <Pagination
              count={6}
              shape="rounded"
              variant="outlined"
              onChange={handleChange}
              className={classes.pagination}
              color="primary"
            />
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export async function getStaticProps() {
  const res = await fetch("http://login.sukhothai.in/route/blogST/blogList");
  const res2 = await fetch(
    "http://login.sukhothai.in/route/webBannerST/getAllWebPagesBannerData"
  );

  const blogsData = await res.json();
  const bannerData = await res2.json();

  if (!bannerData && !blogsData) {
    return { notFound: true };
  }

  return { props: { blogsData, bannerData } };
}
