import { Container } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { ThreeCard, Titleblock } from "../../components";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  banner: {
    width: "100%",
  },
  // b: {
  //   border: "1px solid red",
  // },
}));

const CelebTalk = ({ bannerData, celebTalkData }) => {
  // console.log(`celeb talk bannerData`, bannerData);
  // console.log(`celeb talk celebTalkData`, celebTalkData);
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <img src={bannerData.data[9].bannerImage} className={classes.banner} />
      <Container maxWidth="lg">
        <Titleblock
          title={`${bannerData.data[9].pageHeader}`}
          className={classes.b}
        />
        <ThreeCard data={celebTalkData} celebTalk={true} />
      </Container>
    </div>
  );
};

export async function getStaticProps() {
  const res = await fetch(
    "http://login.sukhothai.in/route/webBannerST/getAllWebPagesBannerData"
  );
  const res2 = await fetch("http://login.sukhothai.in/route/celebTalks");

  const bannerData = await res.json();
  const celebTalkData = await res2.json();

  if (!bannerData && !celebTalkData) {
    return { notFound: true };
  }

  return { props: { bannerData, celebTalkData } };
}
export default CelebTalk;
