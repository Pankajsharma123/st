import React from "react";
import Image from "next/image";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Container } from "@material-ui/core";
import { Titleblock, ThreeCard, Footer, Navbar } from "../../components";

const useStyles = makeStyles({
  root: {
    width: "100%",
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  banner: {
    width: "100%",
  },
  // b: {
  //   border: "1px solid yellow",
  // },
});
const TherapiesPage = ({ therapiesData }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      {/* <Navbar /> */}

      <img
        src="/images/page-banner/health-benefits.jpg"
        alt="therapies"
        className={classes.banner}
      />

      <Container maxWidth="lg">
        <Titleblock
          title="our wellness therapies"
          baseTitle="Unwind in the air of peace and tranquility at our luxury spa"
        />
        <ThreeCard data={therapiesData.result} therapies={true} />
      </Container>
      {/* <Footer /> */}
    </div>
  );
};

export async function getStaticProps(context) {
  const res = await fetch(
    "http://login.sukhothai.in/route/therapiesST/therapies"
  );
  const therapiesData = await res.json();

  if (!therapiesData) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      therapiesData,
    },
  };
}

export default TherapiesPage;
