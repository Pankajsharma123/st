import React from "react";
import Image from "next/image";
import { Container, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Titleblock } from "../../components";

const useStyles = makeStyles({
  root: {
    width: "100%",
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  banner: {
    width: "100%",
  },
  textColor: {
    color: "#eaad4e",
  },
  timepriceTextColor: {
    color: "#ee018c",
  },
  content: {
    fontSize: "18px",
    lineHeight: "25px",
  },
});
const TherapiesLandingPage = ({ therapiesData }) => {
  // console.log(therapiesData);
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <img
        src="/images/page-banner/health-benefits.jpg"
        alt="therapies"
        className={classes.banner}
      />
      <Container maxWidth="lg">
        <Titleblock title={therapiesData.result.name} />
        <Grid container spacing={3}>
          <Grid item xs={8} className={classes.textColor}>
            <div
              dangerouslySetInnerHTML={{
                __html: therapiesData?.result?.description,
              }}
              className={classes.content}
            />
            <Typography className={classes.timepriceTextColor} variant="h6">
              Therapy starts at Rs.{therapiesData?.result?.therapiestPrice} for{" "}
              {therapiesData?.result?.duration}
            </Typography>
          </Grid>
          <Grid item xs={4}>
            <Image
              src={therapiesData.result.image}
              alt="landing image"
              width={500}
              height={300}
            />
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

//called at build time
export async function getStaticPaths() {
  const res = await fetch(
    "http://login.sukhothai.in/route/therapiesST/therapies"
  );
  const therapiesres = await res.json();

  const paths = therapiesres?.result?.map((curr) => ({
    params: { landing: curr.name.replace(/ /g, "-") },
  }));
  return { paths, fallback: false };
}

//called at build time
export async function getStaticProps({ params }) {
  const res = await fetch(
    `http://login.sukhothai.in/route/therapiesST/getTherapiesByName/${params.landing.replace(
      /-/g,
      " "
    )}`
  );
  const therapiesData = await res.json();

  if (!therapiesData) {
    return {
      notFound: true,
    };
  }
  return { props: { therapiesData } };
}
export default TherapiesLandingPage;
