import Image from "next/image";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Container, Grid, Typography, Box } from "@material-ui/core";
import { ThreeCard, Titleblock } from "../../components";

const useStyles = makeStyles({
  root: {
    width: "100%",
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  banner: {
    width: "100%",
  },
  b: {
    //     border: "1px solid red",
  },
  text: {
    color: "#eaad4e",
    display: "flex",
    alignItems: "center",
    //     border: "1px solid red",
  },
  title: {
    margin: "30px 0px",
    textAlign: "center",
    color: "#ee018c",
    //     border: "1px solid red",
  },
});
const OfferLandingPage = ({ offerData }) => {
  const classes = useStyles();

  //   console.log("/*/*/*/*/*/*/");
  //   console.log(offerData);
  return (
    <div className={classes.root}>
      <img
        src="/images/page-banner/health-benefits-1.jpg"
        className={classes.banner}
      />
      <Container maxWidth="lg" className={classes.b}>
        <Titleblock title={offerData.result.title} />
        <Grid container className={classes.b}>
          <Grid item lg={3}>
            <Image src={offerData.result.image} width={350} height={350} />
          </Grid>
          <Grid item lg={9} className={classes.text}>
            <Typography variant="h5">{offerData.result.description}</Typography>
          </Grid>
        </Grid>
        <Box className={classes.title}>
          <Typography variant="h3">Avail The Above Offer At</Typography>
        </Box>
        <ThreeCard data={offerData.result.branch} offers={true} />
      </Container>
    </div>
  );
};

export async function getStaticPaths() {
  const res = await fetch("http://login.sukhothai.in/route/SPAOfferST/list");
  const offerUrl = await res.json();

  const paths = offerUrl?.map((x) => ({
    params: {
      landing: x.title.replace(/ /g, "-"),
    },
  }));
  //   console.log("............................////////////////");
  //   console.log(paths);
  return { paths, fallback: true };
}

export async function getStaticProps({ params }) {
  const res = await fetch(
    `http://login.sukhothai.in/route/SPAOfferST/SPAOffers/${params.landing.replace(
      /-/g,
      " "
    )}`
  );
  const offerData = await res.json();
  //   console.log("/-/-/-/---/-/-/-/-/--");
  //   console.log(offerData);
  return { props: { offerData } };
}

export default OfferLandingPage;
