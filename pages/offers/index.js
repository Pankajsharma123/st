import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Container, Grid } from "@material-ui/core";
import { Card, Titleblock } from "../../components";

const useStyles = makeStyles({
  root: {
    width: "100%",
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  banner: {
    width: "100%",
  },
  // container: {
  //   border: "1px solid blue",
  //   marginBottom: "5px",
  // },
});
const Offers = ({ bannerData, offersData }) => {
  // console.log(bannerData);
  // console.log(offersData);
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <img src={bannerData.data[4].bannerImage} className={classes.banner} />
      <Container maxWidth="lg">
        <Titleblock
          title={bannerData.data[4].pageHeader}
          baseTitle="Offers of the day"
        />
        <Grid container spacing={2}>
          {offersData.map((x, index) => (
            <Grid item lg={3} key={index}>
              <Card
                image={x.image}
                // imageLink={`/offers/${x.offerOnHome
                //   .replace(/ /g, "-")
                //   .replace(/%/g, "-Percent")}`}
                imageLink={`/offers/${x.title.replace(/ /g, "-")}`}
                content={x.description}
                textcenter={true}
                textWithlink={x.telephoneNumber}
                linkhref={`tel:${x.telephoneNumber}`}
              />
            </Grid>
          ))}
        </Grid>
      </Container>
    </div>
  );
};

export async function getStaticProps() {
  const res = await fetch(
    "http://login.sukhothai.in/route/webBannerST/getAllWebPagesBannerData"
  );
  const res2 = await fetch("http://login.sukhothai.in/route/SPAOfferST/list");
  const bannerData = await res.json();
  const offersData = await res2.json();

  if (!(bannerData || offersData)) {
    return { notFound: true };
  }

  return { props: { bannerData, offersData } };
}

export default Offers;
