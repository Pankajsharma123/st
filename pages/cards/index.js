import Image from "next/image";
import Link from "next/link";
import { Container, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import { Titleblock } from "../../components";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  banner: {
    width: "100%",
  },
  image: {
    borderRadius: "15px",
  },
  titletext: {
    color: "white",
    textAlign: "center",
  },
  firstcontent: {
    color: "#bd8d43",
    fontSize: "20px",
    marginTop: "40px",
  },
  gridcontainer: {
    // border: "1px solid yellow",
    display: "flex",
    justifyContent: "center",
  },
  b: {
    // border: "1px solid red",
  },
}));
export default function index({ bannerData }) {
  const classes = useStyles();
  // console.log(`cards bannerData`, bannerData);
  return (
    <div className={classes.root}>
      <img
        src={bannerData.data[3].bannerImage}
        alt="cardbanner"
        className={classes.banner}
      />
      <Container maxWidth="lg" className={classes.b}>
        <Titleblock
          title={bannerData.data[3].pageHeader}
          className={classes.b}
        />
        <Grid container className={classes.gridcontainer}>
          <Grid
            item
            lg={6}
            className={classes.b}
            className={classes.gridcontainer}
          >
            <Link
              href={bannerData.data[8].pageHeader
                .toLowerCase()
                .replace(/ /g, "-")}
            >
              <a>
                <Image
                  src="/images/cards/gift-card.jpg"
                  width={450}
                  height={280}
                />
              </a>
            </Link>
          </Grid>
          <Grid item lg={6} className={classes.gridcontainer}>
            <Link
              href={bannerData.data[7].pageHeader
                .toLowerCase()
                .replace(/ /g, "-")}
            >
              <a>
                <Image
                  src="/images/cards/priority-card.jpg"
                  width={450}
                  height={280}
                />
              </a>
            </Link>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export async function getStaticProps() {
  const res = await fetch(
    "http://login.sukhothai.in/route/webBannerST/getAllWebPagesBannerData"
  );
  const bannerData = await res.json();

  if (!bannerData) {
    return { notFound: true };
  }

  return {
    props: { bannerData },
  };
}
