import React, { useState } from "react";
// import { useStyles } from "./styles.js";
import { Container, Grid } from "@material-ui/core";
import {
  ButtonComponent,
  SelectComponent,
  DatePicker,
  Titleblock,
  Snackbar,
} from "../../components";
import { TextFieldComponent } from "../../components";
import {
  Person,
  Call,
  LocationOn,
  Mail,
  Message,
  DateRange,
  AccessTime,
} from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  banner: {
    width: "100%",
  },
  b: {
    // border: "1px solid red",
  },
  y: {
    border: "1px solid green",
  },

  container: {
    display: "flex",
    flexDirection: "column",
    // border: "1px solid red",
    // alignContent: "center",

    // "& .MuiTextField-root": {     //use to give width and margin to TextField or it is the parent of all TextField
    //   margin: theme.spacing(1),
    //   width: "700px !important",
    // },

    "& label.Mui-focused": {
      color: "#eaad4e",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#eaad4e",
    },
    "& .MuiOutlinedInput-root": {
      //border color before focused
      "& fieldset": {
        borderColor: "#eaad4e",
        borderRadius: "0px 5px 5px 0px",
      },
      "&:hover fieldset": {
        //border color when hover
        borderColor: "#eaad4e",
      },
      "&.Mui-focused fieldset": {
        //border color when focused
        borderColor: "#eaad4e",
      },
    },
    "& .MuiFormLabel-root": {
      //for label when it is in rest state
      color: "#eaad4e",
    },
    "& .MuiInputBase-input": {
      color: "#eaad4e",
    },
    "& .MuiIconButton-root": {
      color: "#eaad4e",
    },
  },

  icon: {
    // border: "1px solid yellow",
    width: "40px",
    height: "40px",
    backgroundColor: "#eaad4e",
    borderRadius: "5px 0px 0px 5px",
  },
  messageicon: {
    width: "40px",
    height: "98px",
    backgroundColor: "#eaad4e",
    borderRadius: "5px 0px 0px 5px",
  },

  selectGrid: {
    display: "flex",
    flexDirection: "row",
    // border: "2px solid red",
    padding: "8px",

    "& .MuiFormControl-marginDense": {
      //to solve date field is misalignation
      marginTop: 0,
    },
  },
  buttonGrid: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "20px",
    marginBottom: "20px",
    // border: "1px solid pink",
  },
  button: {
    background: "#ee018c",
    "&:hover": {
      backgroundColor: "#212529",
    },
  },
}));

//dummy data's
const numberOfPerson = ["1", "2", "3", "4+"]; //constant data
const time = [11, 12, 1, 2, 3, 4, 5, 6, 7, 8, 9]; //for time
const visitingTime = []; //for constant time data
for (let i = 0; i < 10; i++) {
  if (time[i] == 11) {
    visitingTime.push(("0" + time[i].toString()).slice(-2) + ":00 AM");
    visitingTime.push(("0" + time[i].toString()).slice(-2) + ":30 AM");
  } else {
    visitingTime.push(("0" + time[i].toString()).slice(-2) + ":00 PM");
    visitingTime.push(("0" + time[i].toString()).slice(-2) + ":30 PM");
  }
}

const initialValue = {
  fullname: "",
  email: "",
  phoneNumber: "",
  selectedSukhothai: "",
  numberOfPerson: "",
  appointmentDate: null,
  visitingTime: "",
  message: "",
};

const Appointment = ({ bannerData, selectYrST }) => {
  // console.log(`Appointment bannerData`, bannerData);
  // console.log(`selectYrST`, selectYrST);

  const [state, setState] = useState(initialValue);

  const [errors, setErrors] = useState({});

  const [alert, setAlert] = useState(false);

  const selectYourSukhothai = selectYrST.map(
    //data for Select Your Sukho Thai
    (x) => `${x.F_CITY}-Sukhothai ${x.ST_LOCN}`
  );

  const classes = useStyles();

  const handleChange = async (e) => {
    // console.log(`e`, typeof e);
    const { value, name } = e.target;
    setState({ ...state, [name]: value });
    validate({ [name]: value }); //fieldValue take this value
  };

  //validate function
  const validate = (fieldValue = state) => {
    const temp = { ...errors };
    console.log("++++++++++++++++++++++++");
    console.log(`state`, state);
    console.log(`fieldValue`, fieldValue);
    //fullname validation
    if ("fullname" in fieldValue) {
      temp.fullname =
        fieldValue.fullname.trim() == "" ? "Full name is required" : "";
    }

    //email validation
    if ("email" in fieldValue) {
      temp.email =
        fieldValue.email.trim() === ""
          ? "Email is required"
          : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
              fieldValue.email
            )
          ? ""
          : "Invalid email address";
    }
    //phoneNumber validation
    if ("phoneNumber" in fieldValue) {
      temp.phoneNumber =
        fieldValue.phoneNumber.trim() === ""
          ? "Phone Number is required"
          : /^\d{10}$/.test(Number(fieldValue.phoneNumber))
          ? ""
          : "Invalid phone number";
    }

    //selectedSukhothai validation
    if ("selectedSukhothai" in fieldValue) {
      temp.selectedSukhothai =
        fieldValue.selectedSukhothai.length > 0
          ? ""
          : "Please select your Sukho Thai";
    }

    //numberOfPerson validation
    if ("numberOfPerson" in fieldValue) {
      temp.numberOfPerson =
        fieldValue.numberOfPerson.length > 0
          ? ""
          : "Please select number of person";
    }

    //appointmentDate validation
    if ("appointmentDate" in fieldValue) {
      temp.appointmentDate =
        fieldValue.appointmentDate !== null ? "" : "Please select date";
    }

    //visitingTime validation
    if ("visitingTime" in fieldValue) {
      temp.visitingTime =
        fieldValue.visitingTime.trim() === ""
          ? "visiting time is required"
          : "";
    }

    //message validation
    if ("message" in fieldValue) {
      temp.message =
        fieldValue.message.trim() === "" ? "message is required" : "";
    }

    //set the error
    console.log(`temp`, temp);
    setErrors(temp);

    //if fieldValue and state having same ref
    //check if object temp containes any error then return false otherwise true
    if (fieldValue === state) {
      return Object.values(temp).every((x) => x == "");
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate()) {
      // alert(JSON.stringify(state));
      // console.log(state);
      setAlert(true);
      setState(initialValue);
      setErrors({});
    }
  };

  const handleClose = () => {
    setAlert(false);
  };

  return (
    <div className={classes.root}>
      <img
        src={bannerData.data[0].bannerImage}
        alt="appointmentbanner"
        className={classes.banner}
      />
      <Container className={classes.b}>
        <Titleblock title={bannerData.data[0].pageHeader} />
        <form onSubmit={handleSubmit}>
          <Container maxWidth="md">
            <Grid container className={classes.container}>
              <TextFieldComponent
                person={<Person className={classes.icon} />}
                label="Full Name"
                name="fullname"
                value={state.fullname}
                onChange={handleChange}
                placeholder="Enter name"
                variant="outlined"
                size="small"
                isFullWidth={true}
                error={errors.fullname}
              />

              <TextFieldComponent
                mail={<Mail className={classes.icon} />}
                label="Email"
                name="email"
                value={state.email}
                onChange={handleChange}
                placeholder="Enter Email"
                variant="outlined"
                size="small"
                type="email"
                isFullWidth={true}
                error={errors.email}
              />

              <TextFieldComponent
                call={<Call className={classes.icon} />}
                label="Phone Number"
                name="phoneNumber"
                value={state.phoneNumber}
                onChange={handleChange}
                placeholder="Enter Phone Number"
                variant="outlined"
                size="small"
                type="number"
                isFullWidth={true}
                error={errors.phoneNumber}
              />

              <Grid container className={classes.selectGrid} spacing={2}>
                <SelectComponent
                  location={<LocationOn className={classes.icon} />}
                  label="Select Your Sukho Thai"
                  name="selectedSukhothai"
                  value={state.selectedSukhothai}
                  onChange={handleChange}
                  option={selectYourSukhothai}
                  error={errors.selectedSukhothai}
                />
                <SelectComponent
                  person={<Person className={classes.icon} />}
                  label="Select No. of Person"
                  name="numberOfPerson"
                  value={state.numberOfPerson}
                  onChange={handleChange}
                  option={numberOfPerson}
                  error={errors.numberOfPerson}
                />

                <DatePicker
                  date={<DateRange className={classes.icon} />}
                  label="Select Appointment Date"
                  name="appointmentDate"
                  value={state.appointmentDate}
                  onChange={handleChange}
                  error={errors.appointmentDate}
                />

                <SelectComponent
                  accessTime={<AccessTime className={classes.icon} />}
                  label="Select Your Visiting Time"
                  name="visitingTime"
                  value={state.visitingTime}
                  onChange={handleChange}
                  option={visitingTime}
                  error={errors.visitingTime}
                />
              </Grid>

              <TextFieldComponent
                message={<Message className={classes.messageicon} />}
                label="Message"
                name="message"
                value={state.message}
                onChange={handleChange}
                placeholder="Write your message here..."
                variant="outlined"
                size="small"
                type="number"
                isFullWidth={true}
                multiline={true}
                rows={4}
                error={errors.message}
              />

              <Grid item lg={12} className={classes.buttonGrid}>
                <ButtonComponent
                  label="Send"
                  variant="contained"
                  background="#ee018c"
                  hoverBackground="#212529"
                  color="white"
                  width="150px"
                  height="50px"
                  borderRadius="0"
                  type="submit"
                />
              </Grid>
              <Snackbar
                open={alert}
                duration={4000}
                onClose={handleClose}
                severity="success"
                children={
                  "Thank You. Your Appointment will be confirmed shortly"
                }
              />
            </Grid>
          </Container>
        </form>
      </Container>
    </div>
  );
};

export async function getStaticProps() {
  const res1 = await fetch(
    "http://login.sukhothai.in/route/webBannerST/getAllWebPagesBannerData"
  );

  const res2 = await fetch(
    "http://login.sukhothai.in/route/spaLocationST/getLocationListNew"
  );
  const bannerData = await res1.json();
  const selectYrST = await res2.json();

  if (!bannerData && !selectYrST) {
    return { notFound: true };
  }
  return { props: { bannerData, selectYrST } };
}
export default Appointment;
