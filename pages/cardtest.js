import { Card, Carousel, Footer, Bottombar, Navbar } from "../components";

export default function CardTest() {
  return (
    <div>
      <Navbar />
      <Card
        image="https://www.sukhothai.in/wp-content/uploads/2017/12/N5afltxglo7XFY5d.jpeg"
        grow={true}
        titleLink={true}
        title="My BlockTile"
        content={
          "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Maloru The Extremes of Good and Evil) by Cicero, written in 45 BC. "
        }
        link="Read More..."
      />
      <br />

      {/* celebtalk */}
      <Card
        image="https://www.sukhothai.in/wp-content/uploads/2017/12/N5afltxglo7XFY5d.jpeg"
        grow={true}
        date="Dec 20"
        titleLink={true}
        title="My BlockTile"
        content={
          "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Maloru The Extremes of Good and Evil) by Cicero, written in 45 BC. "
        }
        link="Read More..."
      />
      <br />
      <Card
        image="https://www.sukhothai.in/wp-content/uploads/2017/12/N5afltxglo7XFY5d.jpeg"
        title="My BlockTile"
        titleCenter={true}
        content={
          "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Maloru The Extremes of Good and Evil) by Cicero, written in 45 BC. "
        }
      />
      <br />
      {/* Therapie */}
      <Card
        image="https://www.sukhothai.in/wp-content/uploads/2017/12/N5afltxglo7XFY5d.jpeg"
        title="THAI TRADITIONAL FOOT MASSAGE"
        titleCenterWithColor={true}
        content={
          "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Maloru The Extremes of Good and Evil) by Cicero, written in 45 BC. "
        }
        textBelowContent="Therapy starts at Rs.990 for 30 mins"
        link="Read More..."
      />
      <br />

      {/* location */}
      <Card
        image="https://www.sukhothai.in/wp-content/uploads/2017/12/N5afltxglo7XFY5d.jpeg"
        grow={true}
        boldLink="Mumbai"
      />
      <br />

      {/* offercard */}
      <Card
        image="https://www.sukhothai.in/wp-content/uploads/2017/02/264x264_Offer-for-web-feb2017-03.png"
        content={
          "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Maloru The Extremes of Good and Evil) by Cicero, written in 45 BC. "
        }
        textWithlink="9821008877"
        lignSegmentForRoot={true}
      />
      <br />
      <Carousel />
      <br />
      {/* <Footer /> */}
      <br />
      <Bottombar />
      <br />
    </div>
  );
}
