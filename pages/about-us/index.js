import Image from "next/image";
import { Container, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Titleblock } from "../../components";

const useStyles = makeStyles({
  root: {
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  bannerImage: {
    width: "100%",
  },
  para: {
    color: "#eaad4e",
    marginBottom: "20px",
  },
  titleColor: {
    color: "#ee018c",
    margin: "5px 0px",
  },
  images: {
    display: "flex",
    flexDirection: "column",
  },
  img: {
    marginBottom: "10px",
    // border: "1px solid red",
  },
});
export default function AboutUs({ AboutUsData }) {
  const listData = AboutUsData.data[1].subContent[2].subContent
    .slice(22)
    .split("\n")
    .filter((x) => x.trim() != ""); //dividing paragraph based on \n and in result we are getting list
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <img
        src={AboutUsData.data[1].bannerImage}
        className={classes.bannerImage}
      />
      <Container maxWidth="lg">
        <Titleblock title={AboutUsData.data[1].pageHeader} />
        <Grid container spacing={3}>
          <Grid item lg={8}>
            <Typography variant="body1" className={classes.para}>
              {AboutUsData.data[1].subContent[0].subContent}
            </Typography>
            <Typography variant="h5" className={classes.titleColor}>
              {AboutUsData.data[1].subContent[1].subContent.slice(0, 11)}
            </Typography>
            <Typography className={classes.para}>
              {AboutUsData.data[1].subContent[1].subContent.slice(13, 85)}
            </Typography>
            <Typography variant="h5" className={classes.titleColor}>
              {AboutUsData.data[1].subContent[1].subContent.slice(88, 101)}
            </Typography>
            <Typography className={classes.para}>
              {AboutUsData.data[1].subContent[1].subContent.slice(101)}
            </Typography>
            <Typography variant="h5" className={classes.titleColor}>
              {AboutUsData.data[1].subContent[2].subContent.slice(0, 22)}
            </Typography>
            <ul>
              {listData.map((data, index) => (
                <li key={index} className={classes.para}>
                  {data}
                </li>
              ))}
            </ul>
          </Grid>
          <Grid item lg={4} className={classes.images}>
            {AboutUsData.data[1].subContent.map((x, index) => (
              <div className={classes.img} key={index}>
                <Image
                  src={x.subContentImg}
                  alt="about us image"
                  width={500}
                  height={350}
                />
              </div>
            ))}
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export async function getStaticProps() {
  const res = await fetch(
    "http://login.sukhothai.in/route/webBannerST/getAllWebPagesBannerData"
  );
  const AboutUsData = await res.json();

  if (!AboutUsData) {
    return {
      notFound: true,
    };
  }
  return {
    //it will be passed to the page component as a props
    props: {
      AboutUsData,
    },
  };
}
