import { Container, makeStyles, Typography } from "@material-ui/core";
import Image from "next/image";
import { Titleblock } from "../../components";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  headerText: {
    color: "#eaad4e",
    // border: "1px solid red",
    textAlign: "center",
    paddingBottom: "20px",
  },
  tableUpperText: {
    color: "#eaad4e",
    display: "flex",
    justifyContent: "flex-end",
    // border: "1px solid red",
  },
}));
export default function BuyGiftCard({ bannerData }) {
  console.log(`bannerData`, bannerData);

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Image
        src={bannerData[8].bannerImage}
        layout="responsive"
        width={500}
        height={100}
        alt="naturehand jpg pic"
        quality={25}
      />
      <Container maxWidth="lg">
        <Titleblock title={bannerData[8].pageHeader} />
        <Typography variant="h5" className={classes.headerText}>
          {bannerData[8].content.slice(0, 38)}
        </Typography>
        <Typography variant="h5" className={classes.headerText}>
          {bannerData[8].content.slice(39, 96)}
        </Typography>
        <Typography variant="body2" className={classes.headerText}>
          {bannerData[8].content.slice(96)}
        </Typography>
        <Typography variant="caption" className={classes.tableUpperText}>
          *Special Online Discounted Price
        </Typography>
      </Container>
    </div>
  );
}

export async function getStaticProps() {
  const res = await fetch(
    "http://login.sukhothai.in/route/webBannerST/getAllWebPagesBannerData"
  );
  const { data } = await res.json();

  if (!data) {
    return { notFound: true };
  }
  return {
    props: { bannerData: data },
  };
}
