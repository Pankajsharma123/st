import { Container, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Titleblock, ContactCard } from "../../components";
// import { LocationOn, Mail } from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  banner: {
    width: "100%",
  },
  b: {
    //     border: "1px solid red",
  },
  iframe: {
    width: "100%",
    //     height: "100%",
  },
}));
const Contact = ({ bannerData }) => {
  // console.log(`Contact bannerData`, bannerData);
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <img
        src={bannerData.data[6].bannerImage}
        alt="contactBanner"
        className={classes.banner}
      />
      <Container maxWidth="lg">
        <Titleblock
          title={`${bannerData.data[6].pageHeader}`}
          className={classes.b}
        />
        <Grid container spacing={3}>
          <Grid item lg={3}>
            <ContactCard
              locationicon={true}
              address={`Jyoti Sadan, 2A Ground Floor, Shitaladevi Temple Road, Mahim, Mumbai - 400 016`}
            />
          </Grid>
          <Grid item lg={3}>
            <ContactCard
              locationicon={true}
              address={`If you have questions or need additional information,`}
              contact={`+919821008877`}
            />
          </Grid>
          <Grid item lg={6}>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3771.6388494117605!2d72.84088711482313!3d19.0356293581871!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7ced32b8f15e5%3A0xf06edb722a432d6c!2sSukho+Thai+Spa!5e0!3m2!1sen!2sin!4v1482162922082"
              width="600"
              height="250"
              frameborder="0"
              allowfullscreen=""
              className={classes.iframe}
            ></iframe>
          </Grid>

          <Grid item lg={3}>
            <ContactCard
              mailicon={true}
              address={`For Feedback contact`}
              gmail={`feedback@sukhothai.in`}
              contact={`+919870989740`}
            />
          </Grid>
          <Grid item lg={3}>
            <ContactCard
              mailicon={true}
              address={`For Franchise contact`}
              gmail={`sukhothai.ashwin@gmail.com`}
              contact={`+919930922284`}
            />
          </Grid>
          <Grid item lg={3}>
            <ContactCard
              mailicon={true}
              address={`For Marketing & PR contact`}
              gmail={`sukhothaimarketing@sukhothai.in`}
              contact={`+919821602020`}
            />
          </Grid>
          <Grid item lg={3}>
            <ContactCard
              mailicon={true}
              address={`For Career contact`}
              gmail={`hr@sukhothai.in`}
              contact={`+919870989740`}
            />
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export async function getStaticProps() {
  const res = await fetch(
    "http://login.sukhothai.in/route/webBannerST/getAllWebPagesBannerData"
  );

  const bannerData = await res.json();

  if (!bannerData) {
    return { notFound: true };
  }

  return { props: { bannerData } };
}
export default Contact;
