import Head from "next/head";
import NoSsr from "@material-ui/core/NoSsr";
import { Layout } from "../components";
import "../styles/globals.css";

function Sukhothai({ Component, pageProps }) {
  return (
    <NoSsr>
      <Layout {...pageProps}>
        <Head>
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
        </Head>
        <Component {...pageProps} />
      </Layout>
    </NoSsr>
  );
}

export default Sukhothai;
