import { useState } from "react";
import Validation from "./Validation";
import { useRouter } from "next/router";

const initialState = {
  fullname: "",
  email: "",
  phonenumber: "",
  city: "",
  tandc: false,
};
//custom hook
export default function useForm() {
  const [state, setState] = useState(initialState);
  const [error, setError] = useState({});
  const [alert, setAlert] = useState(false);
  const router = useRouter();

  const handleError = (currentFieldValue) => {
    setError(Validation(currentFieldValue));
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    console.log(`name`, name);
    setState({ ...state, [name]: value });
    handleError({ [name]: value }); //passing current field value
  };

  const handleSubmit = (e) => {
    // prevent default submit behaviour
    e.preventDefault();

    //conditions
    if (
      ((state.fullname && state.email && state.phonenumber && state.city) ===
        "" &&
        state.tandc === false) ||
      ((state.fullname && state.email && state.phonenumber && state.city) ===
        "" &&
        state.tandc === true)
    ) {
      return setError(Validation(state));
    }

    if (
      (state.fullname && state.email && state.phonenumber && state.city) !==
        "" &&
      state.tandc === false
    ) {
      return setAlert(true);
    }
    return router.push({
      pathname: "/priority-cards/",
      // query: state,   ///priority-cards?fullname=pankaj&email=test%40gmail.com&phonenumber=01111111111&city=sdfsdfds&tandc=true
      query: { data: state }, ///priority-cards?data=%7B"fullname"%3A"kesari+tours"%2C"email"%3A"test%40gmail.com"%2C"phonenumber"%3A"01111111111"%2C"city"%3A"ertwert"%2C"tandc"%3Atrue%7D
    });
  };

  return { state, handleChange, error, handleSubmit, alert, setAlert };
}
