import React, { useState, useEffect } from "react";
import Link from "next/link";
import { Container, Grid, Typography } from "@material-ui/core";
import {
  ButtonComponent,
  Table,
  TextFieldComponent,
  Titleblock,
  Checkbox,
  Snackbar,
} from "../../components";
import { Person, Call, Mail, LocationOn } from "@material-ui/icons";
// import { useStyles } from "./styles.js";
import useForm from "./useForm";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  banner: {
    width: "100%",
  },
  b: {
    // border: "1px solid red",
  },
  container: {
    border: "1px solid #562858",
  },
  offerText: {
    color: "#eaad4e",
    display: "flex",
    flexDirection: "column",
    textAlign: "center",
    justifyContent: "center",
    //     border: "1px solid yellow",s
  },
  noteAlign: {
    width: "100%",
    // border: "1px solid red",
    color: "#eaad4e",
    display: "flex",
    justifyContent: "flex-end",
    paddingTop: "10px",
    paddingBottom: "10px",
  },
  noteTitle: {
    color: "#ee018c",
  },
  noteText: {
    color: "",
  },

  gridcontainer: {
    display: "flex",
    flexDirection: "row",
    // border: "1px solid red",

    "& label.Mui-focused": {
      color: "#eaad4e",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#eaad4e",
    },
    "& .MuiOutlinedInput-root": {
      //border color before focused
      "& fieldset": {
        borderColor: "#eaad4e",
        borderRadius: "0px 5px 5px 0px",
      },
      "&:hover fieldset": {
        //border color when hover
        borderColor: "#eaad4e",
      },
      "&.Mui-focused fieldset": {
        //border color when focused
        borderColor: "#eaad4e",
      },
    },
    "& .MuiFormLabel-root": {
      //for label when it is in rest state
      color: "#eaad4e",
    },
    "& .MuiInputBase-input": {
      color: "#eaad4e",
    },
    "& .MuiIconButton-root": {
      color: "#eaad4e",
    },
  },

  icon: {
    // border: "1px solid yellow",
    width: "40px",
    height: "40px",
    backgroundColor: "#eaad4e",
    borderRadius: "5px 0px 0px 5px",
  },
  link: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  tNcLink: {
    marginLeft: 0,
    color: "#ee018c",
    "&:hover": {
      color: "#eaad4e",
    },
  },
});

const tableHeader = ["Plan", "Pay Only", "Get Value", "Save", ""]; //dummy header data
export default function PriorityCard({ bannerData, priorityCardAPI }) {
  console.log(`bannerData`, bannerData);
  console.log(`priorityCardAPI`, priorityCardAPI);

  const [selectedValue, setSelectedValue] = useState("");
  const [current, setCurrent] = useState([]);
  const [submit, setSubmit] = useState(true);

  const { state, handleChange, error, handleSubmit, alert, setAlert } =
    useForm();

  console.log(`state page`, state);
  console.log(`alert`, alert);
  // for getting radio selected data
  useEffect(() => {
    setCurrent([
      {
        ...priorityCardAPI[selectedValue],
        planIndex: parseInt(selectedValue) + 1,
      },
    ]);
  }, [selectedValue]);

  //for radio selection
  const handleSelect = (e) => {
    setSelectedValue(e.target.value);
    console.log(selectedValue);
    // console.log(`current`, current);
  };

  //submitted radio
  const submitSelect = () => {
    console.log(`selectedValue`, selectedValue);
    if (selectedValue !== "") {
      return setSubmit(false);
    }
    setSubmit(true);
  };

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <img src={bannerData[7]?.bannerImage} className={classes.banner} />

      <Container maxWidth="md">
        <Titleblock title={bannerData[7].pageHeader} />
        {submit ? (
          <Grid container className={classes.container}>
            <Grid item lg={4} className={classes.offerText}>
              <Typography variant="h3">Save</Typography>
              <Typography variant="h4">
                with Priority Membership Now Life Time Validity
              </Typography>
            </Grid>
            <Grid item lg={8} className={classes.b}>
              <Table
                headData={tableHeader}
                cellData={priorityCardAPI}
                handleSelect={handleSelect}
                selectedValue={selectedValue}
              />
              <ButtonComponent
                label="Submit"
                variant="contained"
                width="100%"
                background="#ee018c"
                hoverBackground="#212529"
                color="white"
                borderRadius="0"
                onClick={submitSelect}
              />
            </Grid>
          </Grid>
        ) : (
          <form onSubmit={handleSubmit}>
            <Grid container className={classes.b}>
              <Table headData={tableHeader.slice(0, -1)} cellData={current} />
              <ButtonComponent
                label="Re-Select Plan"
                variant="contained"
                width="100%"
                background="#ee018c"
                hoverBackground="#212529"
                color="white"
                borderRadius="0"
                onClick={() => setSubmit(true)}
              />
              <Typography className={classes.noteAlign}>
                <small>
                  <span className={classes.noteTitle}>Note: </span>Please fill
                  all the details below which are required for online
                  Transaction.
                </small>
              </Typography>
              <Grid container className={classes.gridcontainer} spacing={2}>
                <TextFieldComponent
                  person={<Person className={classes.icon} />}
                  label="Full Name"
                  name="fullname"
                  value={state.fullname}
                  onChange={handleChange}
                  placeholder="Enter Full Name"
                  variant="outlined"
                  size="small"
                  lg={6}
                  md={6}
                  isFullWidth={true}
                  error={error.fullname}
                />
                <TextFieldComponent
                  mail={<Mail className={classes.icon} />}
                  label="Email"
                  name="email"
                  type="email"
                  value={state.email}
                  onChange={handleChange}
                  placeholder="Enter your Email"
                  variant="outlined"
                  size="small"
                  lg={6}
                  md={6}
                  isFullWidth={true}
                  error={error.email}
                />
                <TextFieldComponent
                  call={<Call className={classes.icon} />}
                  label="Phone Number"
                  name="phonenumber"
                  type="number"
                  value={state.phonenumber}
                  onChange={handleChange}
                  placeholder="Enter Phone Number"
                  variant="outlined"
                  size="small"
                  lg={6}
                  md={6}
                  isFullWidth={true}
                  error={error.phonenumber}
                />
                <TextFieldComponent
                  location={<LocationOn className={classes.icon} />}
                  label="City"
                  name="city"
                  value={state.city}
                  onChange={handleChange}
                  placeholder="Enter Phone Number"
                  variant="outlined"
                  size="small"
                  lg={6}
                  md={6}
                  isFullWidth={true}
                  error={error.city}
                />
              </Grid>
              <Grid item xs={12} className={classes.link}>
                <Checkbox
                  checked={state.tandc}
                  name="tandc"
                  onChange={handleChange}
                  label={"I have read and agree to the"}
                />
                <div className={classes.tNcLink}>
                  <Link href="">
                    <a>Terms of Service & Condition</a>
                  </Link>
                </div>
              </Grid>
              <ButtonComponent
                label="Proceed Card Payment"
                variant="contained"
                background="#ee018c"
                hoverBackground="#212529"
                color="white"
                width="100%"
                height="100%"
                borderRadius="0"
                type="submit"
              />
              {alert && (
                <Snackbar
                  open={alert}
                  duration={4000}
                  onClose={() => setAlert(false)}
                  severity="error"
                  children={"Please select the Terms and Condition"}
                />
              )}
            </Grid>
          </form>
        )}
      </Container>
    </div>
  );
}

export async function getServerSideProps() {
  const res1 = await fetch(
    "http://login.sukhothai.in/route/webBannerST/getAllWebPagesBannerData"
  );
  const res2 = await fetch(
    "http://login.sukhothai.in/route/priorityCard/getPrepaid"
  );

  // it (await res1.json() or await res2.json()) gives me full json but i want only data property from json
  //so that i distructure here and gives another name
  const { data: bannerData } = await res1.json();
  const { result: priorityCardAPI } = await res2.json();

  if (!bannerData && !priorityCardAPI) {                              
    return { notFound: true };
  }
  return { props: { bannerData, priorityCardAPI } };
}

//https://stackoverflow.com/questions/54069253/usestate-set-method-not-reflecting-change-immediately
