export default function Validation(value) {
  let temp = {}; //dummy object

  //fullname validation
  if ("fullname" in value) {
    temp.fullname = value.fullname.trim() === "" ? "Full name is required" : "";
  }
  //email validation
  if ("email" in value) {
    temp.email =
      value.email.trim() === ""
        ? "Email is required"
        : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            value.email
          )
        ? ""
        : "Invalid email address";
  }
  //phonenumber validation
  if ("phonenumber" in value) {
    temp.phonenumber =
      value.phonenumber.trim() == ""
        ? "Phone Number is required"
        : /^\d{10}$/.test(Number(value.phonenumber))
        ? ""
        : "Invalid phone number";
  }
  //city validation
  if ("city" in value) {
    temp.city = value.city.trim() === "" ? "City name is required" : "";
  }

  return temp;
}
