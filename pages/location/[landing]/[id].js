import React from "react";
import { Titleblock, Card, Map, ThreeCard } from "../../../components";
import { Container, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import CallIcon from "@material-ui/icons/Call";

const useStyles = makeStyles({
  root: {
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  banner: {
    width: "100%",
  },
  circle: {
    border: "2px solid #eaad4e",
    borderRadius: "50%",
    width: "100px",
    height: "90px ",
    float: "left",
    backgroundColor: "#311D40",
    display: "flex",
    justifyContent: "center",
    marginRight: "15px",
  },
  circle2: {
    border: "2px solid #eaad4e",
    borderRadius: "50%",
    width: "80px",
    height: "90px ",
    float: "left",
    backgroundColor: "#311D40",
    display: "flex",
    justifyContent: "center",
    marginRight: "15px",
  },
  icons: {
    marginTop: "15px",
    color: "#eaad4e",
    width: "45px",
    height: "50px",
  },
  text: {
    color: "#eaad4e",
    fontSize: "17px",
    // border: "2px solid red",
    display: "flex",
    alignItems: "center",
  },
  contentInColumn: {
    display: "flex",
    flexDirection: "column",
    // border: "2px solid red",
  },
  horizontalalign: {
    display: "flex",
    flexDirection: "row",
    marginBottom: "20px",
    // border: "2px solid red",
  },
  mapdiv: {
    width: "100%",
    height: "100%",
    border: "1px solid red",
  },
  attractionhead: {
    color: "#ee018c",
    textTransform: "uppercase",
    marginTop: "20px",
  },
});
const Landing = ({ locationData }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <img
        src="/images/page-banner/health-benefits.jpg"
        className={classes.banner}
      />
      <Titleblock
        title={`SUKHO THAI ${locationData.result.ST_LOCN} ${locationData.result.F_CITY} - ${locationData.result.ST_BRN}`}
      />
      <Container>
        <Grid container spacing={3}>
          <Grid item lg={4} className={classes.contentInColumn}>
            <div className={classes.horizontalalign}>
              <div className={classes.circle}>
                <LocationOnIcon className={classes.icons} />
              </div>
              <Typography className={classes.text}>
                {locationData.result.ST_ADD}
              </Typography>
            </div>

            <div className={classes.horizontalalign}>
              <div className={classes.circle2}>
                <CallIcon className={classes.icons} />
              </div>
              <Typography className={classes.text}>
                {`+${locationData.result.RECP_MOB}`}
              </Typography>
            </div>

            <Card
              subTitle="Amenities:"
              content={locationData.result.amenities}
            />
          </Grid>
          <Grid item lg={4}>
            {/* <iframe
              _ngcontent-sukhothai-universal-c385=""
              src={locationData.result.ADDR_URL}
              height="100%"
              width="100%"
              title="location map"
              frameborder="0"
              allowfullscreen
            ></iframe> */}
            <Map
              className={classes.mapdiv}
              latitude={Number(locationData.result.latitude)} //convert them into number bcz they are in string
              longitude={Number(locationData.result.longitude)}
              title={`SUKHO THAI ${locationData.result.ST_LOCN}`}
              address={locationData.result.ST_ADD}
              contact={locationData.result.RECP_MOB}
              url={locationData.result.ADDR_URL}
            />
          </Grid>
          <Grid item lg={4}>
            <Card
              image="/images/st-eva.jpg"
              title="ABOUT SUKHOTHAI SPA"
              titleLeft={true}
              content="Established in 2010, Sukho Thai is India's premier Foot Therapy brand inspired from the deeply rooted cultures of Thailand. The seemingly small nation has a great culture and is home to exotic fruits and flowers. The word ‘Sukho’ is derived from the Sanskrit word ‘Sukh’ which connotes happiness and ‘Thai’ reflects its innate bond with Thailand. With over 15 outlets spanning across Mumbai, Pune and Goa, Sukho Thai is the first international foot massages spa chain to open a series of outlets in India."
              textalignleft={true}
            />
          </Grid>

          {locationData.result.attraction.length && (
            <Typography variant="h4" className={classes.attractionhead}>
              {`Attraction near ${locationData.result.F_SHORTADD}`}
            </Typography>
          )}
          {locationData.result.attraction.length && (
            <Grid item lg={12}>
              <ThreeCard
                data={locationData.result.attraction}
                deepInLocation={true}
              />
            </Grid>
          )}
        </Grid>
      </Container>
    </div>
  );
};
export async function getStaticPaths() {
  const res = await fetch(
    "http://login.sukhothai.in/route/spaLocationST/locationList"
  );
  const locationRes = await res.json();

  const paths1 = locationRes?.map((x) => x.DATA.map((y) => y)).flat();
  const paths = paths1.map((x) => ({
    params: { landing: x.F_CITY, id: x.ST_BRN },
  }));
  return { paths, fallback: false };
}
export async function getStaticProps({ params }) {
  const res = await fetch(
    `http://login.sukhothai.in/route/spaLocationST/getDataByBranchCode/${params.id}`
  );

  const locationData = await res.json();
  if (!locationData) {
    return {
      notFound: true,
    };
  }
  return {
    props: { locationData },
  };
}
export default Landing;
