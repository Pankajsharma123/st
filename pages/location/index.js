import React, { useState } from "react";
import { Accordion, Titleblock } from "../../components";
import { makeStyles } from "@material-ui/core/styles";
import { Button, Container, Grid } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  banner: {
    width: "100%",
  },
  buttonContanier: {
    // border: "1px solid yellow",
    display: "flex",
    justifyContent: "flex-end",
  },
  button: {
    backgroundColor: "#ee018c",
    color: "white",
    textTransform: "capitalize",
    borderRadius: "0px",
    "&:hover": {
      backgroundColor: "#1f1229",
    },
  },
});
const Location = ({ LocationData, bannerData }) => {
  // console.log("===========");
  // console.log(LocationData);
  // console.log("+++++++++++++");
  // console.log(bannerData);
  const [expandAll, setExpandAll] = useState(false);
  const classes = useStyles();

  const handleClick = () => setExpandAll(!expandAll);

  return (
    <div className={classes.root}>
      <img src={bannerData.data[2].bannerImage} className={classes.banner} />
      <Container maxWidth="lg">
        <Titleblock title={bannerData.data[2].pageHeader} />
        <Grid container spacing={3}>
          <Grid item lg={12} className={classes.buttonContanier}>
            <Button
              variant="contained"
              size="large"
              className={classes.button}
              onClick={handleClick}
            >
              {expandAll ? "Expand All" : "Collapse All"}
            </Button>
          </Grid>
          <Grid item lg={12}>
            <Accordion
              data={LocationData}
              expandall={expandAll}
              location={true}
            />
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export async function getStaticProps() {
  const res = await fetch(
    "http://login.sukhothai.in/route/spaLocationST/locationList"
  );
  const res1 = await fetch(
    "http://login.sukhothai.in/route/webBannerST/getAllWebPagesBannerData"
  );
  const LocationData = await res.json();
  const bannerData = await res1.json();

  if (!LocationData && !bannerData) {
    return { notFound: true };
  }
  return { props: { LocationData, bannerData } };
}

export default Location;
