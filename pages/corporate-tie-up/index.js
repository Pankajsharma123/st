import Image from "next/image";
import React from "react";
import { Container, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Titleblock } from "../../components";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#311D40",
    paddingBottom: "50px",
  },
  banner: {
    width: "100%",
  },
  image: {
    borderRadius: "15px",
  },
  titletext: {
    color: "white",
    textAlign: "center",
  },
  firstcontent: {
    color: "#bd8d43",
    fontSize: "20px",
    marginTop: "40px",
  },
}));
export default function CorporateTieUp({ bannerData }) {
  const classes = useStyles();
  // console.log(`bannerData`, bannerData);
  return (
    <div className={classes.root}>
      <img src={bannerData.data[10].bannerImage} className={classes.banner} />
      <Container maxWidth="lg">
        <Titleblock title={bannerData.data[10].pageHeader} />
        <Grid container spacing={1}>
          <Grid item lg={6}>
            <Image
              src={bannerData.data[10].subContent[0].subContentImg}
              alt="corporate-tie-up"
              width={650}
              height={400}
              className={classes.image}
            />
          </Grid>
          <Grid item lg={6}>
            <Typography variant="h4" className={classes.titletext}>
              {bannerData.data[10].subContent[0].subContent.slice(0, 19)}
            </Typography>
            <Typography className={classes.firstcontent}>
              {bannerData.data[10].subContent[0].subContent.slice(19, 378)}
            </Typography>
            <Typography className={classes.firstcontent}>
              {bannerData.data[10].subContent[0].subContent.slice(378)}
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export async function getStaticProps() {
  const res = await fetch(
    "http://login.sukhothai.in/route/webBannerST/getAllWebPagesBannerData"
  );
  const bannerData = await res.json();

  if (!bannerData) {
    return { notFound: true };
  }

  return {
    props: { bannerData },
  };
}
