module.exports = {
  images: {
    domains: ["d3r8gwkgo0io6y.cloudfront.net", "d1zx74otwsbzif.cloudfront.net"],
  },

  async redirects() {
    return [
      {
        source: "/",
        destination: "/home",
        permanent: true,
      },
    ];
  },
  // env: {
  //   REACT_APP_GOOGLE_MAP_API_KEYS: process.env.REACT_APP_GOOGLE_MAP_API_KEYS,
  // },
};
// const withBundleAnalyzer = require("@next/bundle-analyzer")({
//   enabled: process.env.ANALYZE === "true",
// });

// module.exports = withBundleAnalyzer({});
